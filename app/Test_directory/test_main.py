import pytest
import requests


@pytest.mark.asyncio
async def test_read_main():
    response = requests.get("/fetch")
    assert response.status_code == 200
    assert response.json() == {"message": "Patients data retrieved successfully"}


