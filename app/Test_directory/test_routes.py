from fastapi.testclient import TestClient
from app.server.routes.SetAppointment_routes import router

client = TestClient(router)


def test_get_patients():
    response = client.get("/fetch/")
    json_response = response.json()
    assert response.status_code == 200
    assert json_response['message'] == "Patients data retrieved successfully"

def test_get_patient_data():
    response = client.get("/603cd15ec9390cbf081d1bbc/")
    json_response = response.json()
    assert response.status_code == 200
    assert json_response['message'] == "Patient data retrieved successfully"


def test_add_patient_data():
    response = client.post("/add",
                           json={
                                   "aptdatetime": "string",
                                   "firstname": "string",
                                   "mobile": "int",
                                   "reason": "string"
                           })
    json_response = response.json()
    assert response.status_code == 200
    assert json_response['message'] == "Patient added successfully."


def test_update_patient_data():
    response = client.put("/users/update/603cd15ec9390cbf081d1bbc",
                          json={
                              "aptdatetime": "string",
                              "firstname": "string",
                              "mobile": "int",
                              "reason": "string"

                          }
                          )
    json_response = response.json()
    assert response.status_code == 200
    assert json_response['message'] == "Patient name updated successfully"
