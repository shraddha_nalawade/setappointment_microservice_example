from pydantic import BaseModel, Field
from typing import Optional


class PatientSchema(BaseModel):
    aptdatetime: str = Field(...)
    firstname: str = Field(...)
    mobile: int = Field(...)
    reason: str = Field(...)


    class Config:

        schema_extra = {
            "example": {
                "aptdatetime": "25/3/2021",
                "firstname": "Shraddha nalawade",
                "mobile": "9373072048",
                "reason": "fever",

            }
        }


class UpdatePatientModel(BaseModel):
    aptdatetime: Optional[str]
    firstname: Optional[str]
    mobile: Optional[int]
    reason: Optional[str]


    class Config:
        schema_extra = {
            "example": {

                "aptdatetime": "25/3/2021",
                "firstname": "Shraddha nalawade",
                "mobile": "9373072048",
                "reason": "fever",
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}
