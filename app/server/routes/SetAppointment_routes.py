from typing import Optional
from fastapi import APIRouter, Body, Depends, Path, Query

from fastapi.encoders import jsonable_encoder
from fastapi_pagination import PaginationParams, Page
from fastapi_pagination.paginator import paginate
from pymongo import MongoClient


from app.server.databse import (
    add_patient,
    delete_patient,
    retrieve_patient,
    update_patient, retrieve_patients, patients_collection,
)
from app.server.models.SetAppointment import (
    ErrorResponseModel,
    ResponseModel,
    PatientSchema,
    UpdatePatientModel,
)


router = APIRouter()


@router.post("/add", response_description="Patient data added into the database")
async def add_patient_data(patient: PatientSchema = Body(...)):
    patient = jsonable_encoder(patient)
    new_patient = await add_patient(patient)
    return ResponseModel(new_patient, "Patient added successfully.")


#@router.get('/users')
#async def list_users():
#    users = await retrieve_patients()
#    if users:
#        return ResponseModel(users, "Users Retrieved Successfully")
#    return ResponseModel(users, "Empty List")


@router.get("/fetch", response_description="Patients retrieved")
async def get_patients(params: PaginationParams = Depends()):
    patients = await retrieve_patients()
    if patients:
        return ResponseModel(paginate(patients,params), "Patients data retrieved successfully")
    return ResponseModel(paginate(patients,params), "Empty list returned")


@router.get("/{id}", response_description="Patient data retrieved")
async def get_patient_data(id):
    patient = await retrieve_patient(id)
    if patient:
        return ResponseModel(patient, "Patient data retrieved successfully")
    return ErrorResponseModel("An error occurred.", 404, "Patient doesn't exist.")


@router.put("/users/update/{id}")
async def update_patient_data(id: str, req: UpdatePatientModel = Body(...)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    updated_patient = await update_patient(id, req)
    if updated_patient:
        return ResponseModel(
            "Patient with ID: {} name update is successful".format(id),
            "Patient name updated successfully",
        )
    return ErrorResponseModel(
        "An error occurred",
        404,
        "There was an error updating the patient data.",
    )


@router.delete("/delete/{id}", response_description="Patient data deleted from the database")
async def delete_patient_data(id: str):
    deleted_patient = await delete_patient(id)
    if deleted_patient:
        return ResponseModel(
            "Patient with ID: {} removed".format(id), "Patient deleted successfully"
        )
    return ErrorResponseModel(
        "An error occurred", 404, "Patient with id {0} doesn't exist".format(id)
    )
