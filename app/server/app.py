from fastapi import FastAPI
from app.server.routes.SetAppointment_routes import router as PatientRouter

app = FastAPI()

app.include_router(PatientRouter, tags=["Patients"], prefix="/users")
@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Welcome to this Set Appointment App!"}