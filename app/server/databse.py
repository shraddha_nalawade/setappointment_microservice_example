from bson.objectid import ObjectId
import motor.motor_asyncio
from pymongo import MongoClient
from sqlalchemy.testing import db
from fastapi_pagination import PaginationParams ,page
from pymongo import MongoClient
from fastapi_pagination.paginator import paginate
from fastapi import APIRouter, Body, Depends, Path, Query
import datetime

MONGO_DETAILS = "mongodb://localhost:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.SetAppointment


patients_collection = database.get_collection("col_SetAppointment")


# helpers


def patient_helper(patient) -> dict:
    return {
        "id": str(patient["_id"]),
        "aptdatetime": patient["aptdatetime"],
        "firstname": patient["firstname"],
        "mobile": patient["mobile"],
        "reason": patient["reason"]

    }


# Retrieve all patients present in the database
async def retrieve_patients():
    #try:
    patients = []
    async for patient in patients_collection.find():
        patients.append(patient_helper(patient))
    return  patients
    #return paginate(patients,params)
        #return {"status_code": 200, "message": "Success", "Extra_message": "Data Coming From DB",
                #"patient_data": paginate(patients, params)}

    #except:
        #return {"status_code": 404, "message": "Failed", "Extra_message": "User Not Found with this Id"}

# Add a new patient into to the database
async def add_patient(patient_data: dict) -> dict:
    patient = await patients_collection.insert_one(patient_data)
    new_patient = await patients_collection.find_one({"_id": patient.inserted_id})
    return patient_helper(new_patient)


# Retrieve a patient with a matching ID
async def retrieve_patient(id: str) -> dict:
    patient = await patients_collection.find_one({"_id": ObjectId(id)})
    if patient:
        return patient_helper(patient)


# Update a patient with a matching ID
async def update_patient(id: str, data: dict):
    # Return false if an empty request body is sent.
    if len(data) < 1:
        return False
    patient = await patients_collection.find_one({"_id": ObjectId(id)})
    if patient:
        updated_patient = await patients_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_patient:
            return True
        return False


# Delete a patient from the database
async def delete_patient(id: str):
    patient = await patients_collection.find_one({"_id": ObjectId(id)})
    if patient:
        await patients_collection.delete_one({"_id": ObjectId(id)})
        return True
